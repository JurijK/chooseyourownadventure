﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using CYOA.Library.Handlers;
using CYOA.Library.Objects;

namespace CYOA.Library.Tests.HandlersTests
{
    public class PagesTests
    {
        private Adventure _adventure;

        [SetUp]
        public void SetUp()
        {
            _adventure = Adventures.GetAdventure(0); //test adventure
        }

        [Test]
        public void GetPage_WhenCalledWithInvalidIndex_WillReturnNull()
        {
            Assert.IsNull(Pages.GetPage(_adventure, -1));
        }

        [Test]
        public void GetPage_WhenCalledWithInvalidCode_WillReturnNull()
        {
            Assert.IsNull(Pages.GetPage(_adventure, ""));
        }

        [Test]
        [Ignore("Already tested and working")]
        public void CreatePage_WhenCalledWithTestParameters_WillReturnNewlyCreatedPage()
        {
            Assert.IsInstanceOf<Page>(Pages.CreatePage(_adventure, "testDevCode", "Lorem Ipsum dolor sit amet"));
        }

        [Test]
        public void CreatePage_WhenCalledWithParametersOfExistingPage_WillThrowException()
        {
            Assert.Throws<ArgumentException>(() => Pages.CreatePage(_adventure, "testDevCode", "Lorem Ipsum dolor sit amet"));
        }

        [Test]
        public void EditPage_WhenCalledWithNewContentParameter_WillReturnPageWithChangedContent()
        {
            string newContent = "Test content";

            Page page = Pages.GetPage(_adventure, "testDevCode");

            Assert.AreEqual(newContent, Pages.EditPage(_adventure, page, newContent).Content);
        }
    }
}
