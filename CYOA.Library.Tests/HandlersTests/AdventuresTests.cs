﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using CYOA.Library.Handlers;

namespace CYOA.Library.Tests.HandlersTests
{
    public class AdventuresTests
    {
        [Test]
        public void GetAdventure_WhenCalledWithInvalidIndex_WillReturnNull()
        {
            Assert.IsNull(Adventures.GetAdventure(-1));
        }

        [Test]
        public void GetAdventure_WhenCalledWithNotExistingCode_WillReturnNull()
        {
            Assert.IsNull(Adventures.GetAdventure("Lorem Ipsum"));
        }

        [Test]
        [Ignore("Already tested and working")]
        public void CreateAdventure_WhenCalledWithTestParameters_WillReturnNewlyCreatedAdventure()
        {
            Assert.IsInstanceOf<Objects.Adventure>(Adventures.CreateAdventure("Test author", "Test", "PL", 18));
        }

        [Test]
        public void CreateAdventure_WhenCalledWithParametersOfExistingAdventure_WillThrowArgumentException()
        {
            Assert.Throws<ArgumentException>(() => Adventures.CreateAdventure("Test author", "Test", "PL", 18));
        }
    }
}
