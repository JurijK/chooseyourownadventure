﻿using CYOA.Library.Handlers;
using CYOA.Library.Objects;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYOA.Library.Tests.HandlersTests
{
    public class ChoicesTests
    {
        private Adventure _adventure;
        private Page _page;

        [SetUp]
        public void SetUp()
        {
            _adventure = Adventures.GetAdventure(0); //test adventure
            _page = Pages.GetPage(_adventure, 1);
        }

        [Test]
        public void GetChoice_WhenCalledWithInvalidIndex_WillReturnNull()
        {
            Assert.IsNull(Choices.GetChoice(_page, -1));
        }

        [Test]
        public void GetRandomChoice_WhenCalledWithNewPage_WillReturnLastChoice()
        {
            List<Choice> choices = new List<Choice>()
            {
                new Choice()
                {
                    Id = 0,
                    Content = "",
                    PointsToPageWithId = 1,
                    Weight = 100000
                },
                new Choice()
                {
                    Id = 1,
                    Content = "",
                    PointsToPageWithId = 1,
                    Weight = 0
                }
            };

            Page page = new Page()
            {
                Id = 0,
                DevelopmentCode = "",
                ImageName = "",
                SoundName = "",
                AreChoicesRandom = false,
                Content = "",
                Choices = choices
            };

            Assert.AreEqual(0, Choices.GetRandomChoice(page).Id);
        }

        [Test]
        public void GetRandomChoice_WhenCalledConstantlyWithThreeEquailyLikelyChoices_LastChoiceWillBeShownin33PercentOfCases()
        {
            List<Choice> choices = new List<Choice>()
            {
                new Choice()
                {
                    Id = 0,
                    Content = "",
                    PointsToPageWithId = 1,
                    Weight = 10
                },
                new Choice()
                {
                    Id = 1,
                    Content = "",
                    PointsToPageWithId = 1,
                    Weight = 10
                },
                new Choice()
                {
                    Id = 2,
                    Content = "",
                    PointsToPageWithId = 1,
                    Weight = 10
                }
            };

            Page page = new Page()
            {
                Id = 0,
                DevelopmentCode = "",
                ImageName = "",
                SoundName = "",
                AreChoicesRandom = false,
                Content = "",
                Choices = choices
            };

            int lastCounter = 0;

            for (int i = 0; i < 10000; i++)
            {
                if (Choices.GetRandomChoice(page).Id == 2)
                {
                    lastCounter++;
                }
            }

            Assert.AreEqual(0.33, (double)lastCounter/10000.0, 0.02);
        }

        [Test]
        [Ignore("Already tested and working")]
        public void CreateChoice_WhenCalledWithTestParameters_WillReturnNewlyCreatedChoice()
        {
            Assert.IsInstanceOf<Choice>(Choices.CreateChoice(_adventure, _page, "Lorem Ipsum", 2));
        }

        [Test]
        public void CreateChoice_WhenCalledWithInvalidParameters_ThrowsArgumentNullExeption()
        {
            Assert.Throws<ArgumentNullException>(() => Choices.CreateChoice(_adventure, _page, "Lorem Ipsum", 0));
        }

        [Test]
        public void EditChoice_WhenCalledWithNewParameters_ReturnsEditedChoice()
        {
            string newContent = "Test choice content";

            Choice choice = Choices.GetChoice(_page, 1);

            Assert.AreEqual(newContent, Choices.EditChoice(_adventure, _page, choice, newContent, 2).Content);
        }

        [Test]
        public void EditChoice_WhenCalledWithPointerToItsOwnPage_ThrowsArgumentException()
        {
            string newContent = "Test choice content";

            Choice choice = Choices.GetChoice(_page, id: 1);

            Assert.Throws<ArgumentException>(() => Choices.EditChoice(_adventure, _page, choice, newContent, newPointsToPageWithId: 1));
        }

        [Test]
        public void EditChoice_WhenCalledWithInvalidParameters_ThrowsArgumentNullExeption()
        {
            Choice choice = Choices.GetChoice(_page, id: 1);
            Assert.Throws<ArgumentNullException>(() => Choices.EditChoice(_adventure, _page, choice, newContent: "Lorem Ipsum", newPointsToPageWithId: 0));
        }
    }
}
