﻿using CYOA.Library.Handlers;
using CYOA.Library.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace CYOA.DesktopClientApp
{
    public class Utils
    {
        public const string EmailContact = "bajarzdevelopment@gmail.com";
        public const string DiscordInvite = "discord.gg/TjCDEQU";
        public const string AppDiscordId = "805724062235623445";
        public const string AppVersion = "3.0.1.0";

        public static void HandleException(Exception ex, Window window, bool returnToMainMenu = true)
        {
            MessageBox.Show($@"ERROR! Unhandled exception occured:
{ex}

If you are not sure what go wrong, report this error to us:
E-mail: {EmailContact}
Discord: {DiscordInvite}

{(returnToMainMenu ? "Application will return to main menu." : "")}");

            if (returnToMainMenu)
            {
                MainWindow main = new MainWindow()
                {
                    Top = window.Top,
                    Left = window.Left,
                    Foreground = window.Foreground,
                    Background = window.Background,
                    WindowState = window.WindowState
                };
                main.Show();
            }

            window.Close();
        }

        public static string PerParsePageContent(string pageContent, Adventure adventure)
        {
            pageContent = ParseRecommendedBody(pageContent);

            pageContent = pageContent.Replace("\r\n", @"

");

            pageContent = ParseColors(pageContent);

            pageContent = ParseImagesPath(pageContent, adventure);

            pageContent = ParseMCGenderSpecificWords(pageContent, adventure.IsMCFemale ?? false); //Default male

            pageContent = ParseMCName(pageContent, adventure.MCName);

            return pageContent;
        }

        private static string ParseMCName(string content, string name)
        {
            return content.Replace("%PlayerName%", name);
        }

        private static string ParseMCGenderSpecificWords(string content, bool isFemale)
        {
            foreach (Match match in Regex.Matches(content, @"\%Gender\(([^)]*)\)\%"))
            {
                GroupCollection keyValuePairs = match.Groups;
                string[] genderSpecific = keyValuePairs[1].Value.Replace(" ", "").Split(',');
                content = content.Replace(keyValuePairs[0].Value, isFemale ? genderSpecific[1] : genderSpecific[0]);
            }

            return content;
        }

        private static string ParseImagesPath(string content, Adventure adventure)
        {
            return content.Replace("%ImageFolder%", GetImagesFolderPath(adventure));
        }

        private static string ParseColors(string content)
        {
            return content.Replace("%BackgroundColor%", Profiles.GetProfileBackgroundColor())
                          .Replace("%FontColor%", Profiles.GetProfileFontColor());
        }

        private static string ParseRecommendedBody(string content)
        {
            if (content.Contains("%Body%"))
            {
                return $"<body style=\"background-color: %BackgroundColor%; color: %FontColor%; text-align: center; font-size:13\">\r\n{content.Replace("%Body%", "")}\r\n</body>";
            }

            return content;
        }

        public static string GetImagesFolderPath(Adventure adventure)
        {
            return $@"{AppDomain.CurrentDomain.BaseDirectory}/Resources/Stories/{adventure.Code}/Images/";
        }

        public static string GetSoundsFolderPath(Adventure adventure)
        {
            return $@"{AppDomain.CurrentDomain.BaseDirectory}/Resources/Stories/{adventure.Code}/Sounds/";
        }
    }
}
