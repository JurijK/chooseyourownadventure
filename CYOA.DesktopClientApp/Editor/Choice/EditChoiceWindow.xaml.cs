﻿using CYOA.Library.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CYOA.DesktopClientApp.Editor.Choice
{
    /// <summary>
    /// Interaction logic for EditChoiceWindow.xaml
    /// </summary>
    public partial class EditChoiceWindow : Window
    {
        private Library.Objects.Choice _selectedChoice;
        private Library.Objects.Page _selectedPage;
        private Library.Objects.Adventure _selectedAdventure;

        public EditChoiceWindow()
        {
            InitializeComponent();

            AdventuresComboBox.ItemsSource = Adventures.GetAdventures();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Choices.EditChoice(_selectedAdventure, _selectedPage, _selectedChoice, Content.Text, ((Library.Objects.Page)PointsToComboBox.SelectedItem).Id,
                                   int.Parse(Weight.Text));
            }
            catch (Exception ex)
            {
                Utils.HandleException(ex, this);
                return;
            }

            MainWindow main = new MainWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            main.Show();
            this.Close();
        }

        private void AdventuresComboBox_Selected(object sender, RoutedEventArgs e)
        {
            try
            {
                _selectedAdventure = (Library.Objects.Adventure)AdventuresComboBox.SelectedItem;
                PagesComboBox.ItemsSource = _selectedAdventure.Pages;
            }
            catch (Exception ex)
            {
                Utils.HandleException(ex, this);
                return;
            }
        }

        private void PagesComboBox_Selected(object sender, RoutedEventArgs e)
        {
            try
            {
                _selectedPage = (Library.Objects.Page)PagesComboBox.SelectedItem;
                ChoicesComboBox.ItemsSource = _selectedPage.Choices;
                if (_selectedPage.AreChoicesRandom)
                {
                    WeightLabel.Visibility = Visibility.Visible;
                    Weight.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                Utils.HandleException(ex, this);
                return;
            }
        }

        private void ChoicesComboBox_Selected(object sender, RoutedEventArgs e)
        {
            try
            {
                _selectedChoice = (Library.Objects.Choice)ChoicesComboBox.SelectedItem;
                Content.Text = _selectedChoice.Content;
                Weight.Text = _selectedChoice.Weight.ToString();
                PointsToComboBox.ItemsSource = _selectedAdventure.Pages;
                PointsToComboBox.SelectedItem = _selectedAdventure.Pages.Single(p => p.Id == _selectedChoice.PointsToPageWithId);
            }
            catch (Exception ex)
            {
                Utils.HandleException(ex, this);
                return;
            }
        }

        private void ReturnButton_Click(object sender, RoutedEventArgs e)
        {
            EditorWindow editor = new EditorWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            editor.Show();
            this.Close();
        }
    }
}
