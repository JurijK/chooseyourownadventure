﻿using CYOA.Library.Handlers;
using CYOA.Library.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CYOA.DesktopClientApp.Editor.Choice
{
    /// <summary>
    /// Interaction logic for CreateChoiceWindow.xaml
    /// </summary>
    public partial class CreateChoiceWindow : Window
    {
        private Library.Objects.Page _selectedPage;
        private Library.Objects.Adventure _selectedAdventure;

        public CreateChoiceWindow()
        {
            InitializeComponent();

            AdventuresComboBox.ItemsSource = Adventures.GetAdventures();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Choices.CreateChoice(_selectedAdventure, _selectedPage, Content.Text,
                 ((Library.Objects.Page)PointsToComboBox.SelectedItem).Id,
                 string.IsNullOrWhiteSpace(Weight.Text) ? 10 : int.Parse(Weight.Text.Trim()));
            }
            catch (Exception ex)
            {
                Utils.HandleException(ex, this);
                return;
            }

            MainWindow main = new MainWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            main.Show();
            this.Close();
        }

        private void AdventuresComboBox_Selected(object sender, RoutedEventArgs e)
        {
            try
            {
                _selectedAdventure = (Library.Objects.Adventure)AdventuresComboBox.SelectedItem;
                PagesComboBox.ItemsSource = _selectedAdventure.Pages;
                PointsToComboBox.ItemsSource = _selectedAdventure.Pages;
            }
            catch (Exception ex)
            {
                Utils.HandleException(ex, this);
                return;
            }
        }

        private void PagesComboBox_Selected(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                _selectedPage = (Library.Objects.Page)PagesComboBox.SelectedItem;
                if (_selectedPage.AreChoicesRandom)
                {
                    WeightLabel.Visibility = Visibility.Visible;
                    Weight.Visibility = Visibility.Visible;
                }
                else
                {
                    WeightLabel.Visibility = Visibility.Collapsed;
                    Weight.Visibility = Visibility.Collapsed;
                }
            }
            catch (Exception ex)
            {
                Utils.HandleException(ex, this);
                return;
            }
        }

        private void ReturnButton_Click(object sender, RoutedEventArgs e)
        {
            EditorWindow editor = new EditorWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            editor.Show();
            this.Close();
        }
    }
}
