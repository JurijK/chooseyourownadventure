﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CYOA.DesktopClientApp.Editor.Adventure;
using CYOA.DesktopClientApp.Editor.Choice;
using CYOA.DesktopClientApp.Editor.Page;

namespace CYOA.DesktopClientApp.Editor
{
    /// <summary>
    /// Interaction logic for EditorWindow.xaml
    /// </summary>
    public partial class EditorWindow : Window
    {
        public EditorWindow()
        {
            InitializeComponent();
            DiscordHandler.UpdateRichPresence("Creating own adventure", $"");
        }

        private void CreateAdventure_Click(object sender, RoutedEventArgs e)
        {
            CreateAdventureWindow createAdventure = new CreateAdventureWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            createAdventure.Show();
            this.Close();
        }

        private void CreatePage_Click(object sender, RoutedEventArgs e)
        {
            CreatePageWindow createPage = new CreatePageWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            createPage.Show();
            this.Close();
        }

        private void EditPage_Click(object sender, RoutedEventArgs e)
        {
            EditPageWindow editPage = new EditPageWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            editPage.Show();
            this.Close();
        }

        private void CreateChoice_Click(object sender, RoutedEventArgs e)
        {
            CreateChoiceWindow choiceWindow = new CreateChoiceWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            choiceWindow.Show();
            this.Close();
        }

        private void EditChoice_Click(object sender, RoutedEventArgs e)
        {
            EditChoiceWindow editWindow = new EditChoiceWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            editWindow.Show();
            this.Close();
        }

        private void ReturnButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            main.Show();
            this.Close();
        }
    }
}
