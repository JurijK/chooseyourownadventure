﻿using CYOA.Library.Handlers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CYOA.DesktopClientApp.Editor.Page
{
    /// <summary>
    /// Interaction logic for EditPageWindow.xaml
    /// </summary>
    public partial class EditPageWindow : Window
    {
        private Library.Objects.Page _selectedPage;
        private Library.Objects.Adventure _selectedAdventure;

        public EditPageWindow()
        {
            InitializeComponent();

            AdventuresComboBox.ItemsSource = Adventures.GetAdventures();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double volume;

                try
                {
                    volume = double.Parse(Volume.Text, CultureInfo.InvariantCulture);
                }
                catch (Exception)
                {
                    volume = 0.5;
                }

                Pages.EditPage(_selectedAdventure, _selectedPage, PageContent.Text, DevelopmentCode.Text, Image.Text, Sound.Text,
                               volume, RandomChoices.IsChecked ?? false, RandomChoiceContent.Text);
            }
            catch (Exception ex)
            {
                Utils.HandleException(ex, this);
                return;
            }

            MainWindow main = new MainWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            main.Show();
            this.Close();
        }

        private void AdventuresComboBox_Selected(object sender, RoutedEventArgs e)
        {
            try
            {
                _selectedAdventure = (Library.Objects.Adventure)AdventuresComboBox.SelectedItem;
                PagesComboBox.ItemsSource = _selectedAdventure.Pages;

                if (string.IsNullOrWhiteSpace(_selectedAdventure.ForAppVersion))
                {
                    Adventures.EditAppVersion(_selectedAdventure, Utils.AppVersion);
                }
                else if (_selectedAdventure.ForAppVersion[0] == '2')
                {
                    scrViewer.Visibility = Visibility.Visible;
                    browser.Visibility = Visibility.Hidden;
                    ((Markdown.Xaml.Markdown)this.Resources["Markdown"]).ImageFolderPath = Utils.GetImagesFolderPath(_selectedAdventure);
                }
            }
            catch (Exception ex)
            {
                Utils.HandleException(ex, this);
                return;
            }
        }

        private void PagesComboBox_Selected(object sender, RoutedEventArgs e)
        {
            try
            {
                _selectedPage = (Library.Objects.Page)PagesComboBox.SelectedItem;
                DevelopmentCode.Text = _selectedPage.DevelopmentCode;
                Image.Text = _selectedPage.ImageName;
                Sound.Text = _selectedPage.SoundName;
                Volume.Text = _selectedPage.SoundVolume.ToString(CultureInfo.InvariantCulture);
                RandomChoices.IsChecked = _selectedPage.AreChoicesRandom;
                RandomChoiceContent.Text = _selectedPage.RandomChoiceContent;
                PageContent.Text = _selectedPage.Content;
            }
            catch (Exception ex)
            {
                Utils.HandleException(ex, this);
                return;
            }
        }

        private void RandomChoices_Checked(object sender, RoutedEventArgs e)
        {
            RandomChoiceContentText.Visibility = Visibility.Visible;
            RandomChoiceContent.Visibility = Visibility.Visible;
        }

        private void RandomChoices_Unchecked(object sender, RoutedEventArgs e)
        {
            RandomChoiceContentText.Visibility = Visibility.Collapsed;
            RandomChoiceContent.Visibility = Visibility.Collapsed;
        }

        private void ReturnButton_Click(object sender, RoutedEventArgs e)
        {
            EditorWindow editor = new EditorWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            editor.Show();
            this.Close();
        }
    }
}
