﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CYOA.Library.Handlers;

namespace CYOA.DesktopClientApp.Editor.Adventure
{
    /// <summary>
    /// Interaction logic for CreateAdventureWindow.xaml
    /// </summary>
    public partial class CreateAdventureWindow : Window
    {
        public CreateAdventureWindow()
        {
            InitializeComponent();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Adventures.CreateAdventure(Author.Text, Title.Text, Language.Text, int.Parse(MinimalAge.Text), MCName.Text, MCFemale.IsChecked ?? false,
                                           Music.Text, double.Parse(Volume.Text, CultureInfo.InvariantCulture), Version.Text);
            }
            catch (Exception ex)
            {
                Utils.HandleException(ex, this);
                return;
            }

            MainWindow main = new MainWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            main.Show();
            this.Close();
        }

        private void ReturnButton_Click(object sender, RoutedEventArgs e)
        {
            EditorWindow editor = new EditorWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            editor.Show();
            this.Close();
        }
    }
}
