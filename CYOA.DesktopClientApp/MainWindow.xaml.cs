﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CYOA.DesktopClientApp.Editor;
using CYOA.DesktopClientApp.Game;
using CYOA.Library.Handlers;

namespace CYOA.DesktopClientApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            BrushConverter converter = new BrushConverter();
            this.Foreground = converter.ConvertFromString(Profiles.GetProfileFontColor()) as SolidColorBrush;
            this.Background = converter.ConvertFromString(Profiles.GetProfileBackgroundColor()) as SolidColorBrush;
            InitializeComponent();
            DiscordHandler.Initialize();
            DiscordHandler.UpdateRichPresence("In main menu", $"");
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            AdventureSelectWindow select = new AdventureSelectWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            select.Show();
            this.Close();
        }

        private void EditorButton_Click(object sender, RoutedEventArgs e)
        {
            EditorWindow editor = new EditorWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState,
            };
            editor.Show();
            this.Close();
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            SettingsWindow settings = new SettingsWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState,
            };
            settings.Show();
            this.Close();
        }
    }
}
