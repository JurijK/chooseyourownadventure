﻿using CYOA.Library.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CYOA.DesktopClientApp.Game
{
    /// <summary>
    /// Interaction logic for AdventureSelectWindow.xaml
    /// </summary>
    public partial class AdventureSelectWindow : Window
    {
        private Library.Objects.Adventure _adventure;

        public AdventureSelectWindow()
        {
            InitializeComponent();

            AdventuresComboBox.ItemsSource = Adventures.GetAdventures();
            DiscordHandler.UpdateRichPresence("Selecting adventure", $"");
        }

        private void ConfirmButton_Click(object sender, RoutedEventArgs e)
        {
            if (!ConfirmedNSFW())
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(_adventure.MCName) || !_adventure.IsMCFemale.HasValue)
            {
                AdditionalInformationWindow additional = new AdditionalInformationWindow(_adventure)
                {
                    Top = this.Top,
                    Left = this.Left,
                    Foreground = this.Foreground,
                    Background = this.Background,
                    WindowState = this.WindowState,
                };

                try
                {
                    additional.Show();
                }
                catch (Exception ex)
                {
                    Utils.HandleException(ex, this, false);
                }

                this.Close();

                return;
            }

            GameWindow game = new GameWindow(_adventure)
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };

            try
            {
                game.Show();
            }
            catch (Exception ex)
            {
                Utils.HandleException(ex, this, false);
            }
            
            this.Close();
        }

        private bool ConfirmedNSFW()
        {
            if (_adventure.MinimalAge < 18)
            {
                return true;
            }

            MessageBoxResult result = MessageBox.Show(@"This story is marked as NSFW therefore can contain disturbing or sexual content.

Do you want to proceed?", "Story NSFW", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                return true;
            }

            return false;
        }

        private void ReturnButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background
            };
            main.Show();
            this.Close();
        }

        private void AdventuresComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                _adventure = (Library.Objects.Adventure)AdventuresComboBox.SelectedItem;
                Title.Text = $"Title: {_adventure.Name}";
                Author.Text = $"Author: {_adventure.Author}";
                Lang.Text = $"Language: {_adventure.Language}";
                MinimalAge.Text = $"Minimal age: {((_adventure.MinimalAge >= 18) ? $"{_adventure.MinimalAge}!!!" : _adventure.MinimalAge)}";
            }
            catch (Exception ex)
            {
                Utils.HandleException(ex, this);
                return;
            }
        }
    }
}
