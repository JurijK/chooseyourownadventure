﻿using CYOA.Library.Handlers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CYOA.DesktopClientApp.Game
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Window
    {
        private double? _ambientVolume = null;
        private double? _soundVolume = null;
        private Library.Objects.Page _page;
        private readonly Library.Objects.Adventure _adventure;
        private readonly MediaPlayer _ambientMusicPlayer;
        private readonly MediaPlayer _soundEffectsPlayer;

        public GameWindow()
        {
            _soundEffectsPlayer = new MediaPlayer();
            InitializeComponent();
        }

        public GameWindow(Library.Objects.Adventure adventure) : this()
        {
            try
            {
                _adventure = adventure;

                if (string.IsNullOrWhiteSpace(_adventure.ForAppVersion))
                {
                    Adventures.EditAppVersion(_adventure, Utils.AppVersion);
                }
                else if (_adventure.ForAppVersion[0] == '2')
                {
                    scrViewer.Visibility = Visibility.Visible;
                    browser.Visibility = Visibility.Hidden;
                }

                ((Markdown.Xaml.Markdown)this.Resources["Markdown"]).ImageFolderPath = Utils.GetImagesFolderPath(_adventure);
                Title.Text = _adventure.Name;
                DiscordHandler.UpdateRichPresence(_adventure.Name, $"Adventure by {_adventure.Author}");

                UserAmbientVolume.Text = _adventure.AmbientVolume.ToString(CultureInfo.InvariantCulture);

                _ambientMusicPlayer = new MediaPlayer()
                {
                    Volume = _adventure.AmbientVolume
                };
                if (!string.IsNullOrWhiteSpace(_adventure.AmbientMusic))
                {
                    _ambientMusicPlayer.Stop();
                    _ambientMusicPlayer.Open(new Uri($"{Utils.GetSoundsFolderPath(_adventure)}{_adventure.AmbientMusic}", UriKind.Absolute));
                    _ambientMusicPlayer.Play();
                }
                UpdatePage(1);
            }
            catch (Exception ex)
            {
                Utils.HandleException(ex, this);
                _soundEffectsPlayer.Stop();
                _ambientMusicPlayer.Stop();
                return;
            }
        }

        private void ApplyVolumeChanges_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _ambientVolume = double.Parse(UserAmbientVolume.Text, CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                _ambientVolume = _adventure.AmbientVolume;
            }

            _ambientMusicPlayer.Volume = _ambientVolume.Value;

            try
            {
                _soundVolume = double.Parse(UserSoundsVolume.Text, CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {

            }

            if (_soundVolume.HasValue)
            {
                _soundEffectsPlayer.Volume = _soundVolume.Value;
            }
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            _soundEffectsPlayer.Stop();
            _ambientMusicPlayer.Stop();
            MainWindow main = new MainWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            main.Show();
            this.Close();
        }

        private void Choice1_Click(object sender, RoutedEventArgs e)
        {
            if (_page.AreChoicesRandom)
            {
                UpdatePage(Choices.GetRandomChoice(_page).PointsToPageWithId);
            }
            else
            {
                UpdatePage(_page.Choices[0].PointsToPageWithId);
            }
        }

        private void Choice2_Click(object sender, RoutedEventArgs e)
        {
            UpdatePage(_page.Choices[1].PointsToPageWithId);
        }

        private void Choice3_Click(object sender, RoutedEventArgs e)
        {
            UpdatePage(_page.Choices[2].PointsToPageWithId);
        }

        private void Choice4_Click(object sender, RoutedEventArgs e)
        {
            UpdatePage(_page.Choices[3].PointsToPageWithId);
        }

        private void UpdatePage(int id)
        {
            _page = Pages.GetPage(_adventure, id);
            editSource.Text = Utils.PerParsePageContent(_page.Content, _adventure);

            _soundEffectsPlayer.Stop();
            if (!string.IsNullOrWhiteSpace(_page.SoundName))
            {
                _soundEffectsPlayer.Open(new Uri($"{Utils.GetSoundsFolderPath(_adventure)}{_page.SoundName}", 
                                         UriKind.Absolute));
                _soundEffectsPlayer.Volume = _soundVolume.HasValue ? _soundVolume.Value : _page.SoundVolume;
                _soundEffectsPlayer.Play();
            }

            if (!string.IsNullOrWhiteSpace(_page.ImageName))
            {
                Image.Visibility = Visibility.Collapsed;
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri($@"{Utils.GetImagesFolderPath(_adventure)}{_page.ImageName}",
                                           UriKind.Absolute);
                bitmap.EndInit();
                Image.Source = bitmap;
                Image.Visibility = Visibility.Visible;
            }

            scrViewer.ScrollToTop();

            //To try catch
            Choice1.Visibility = Visibility.Hidden;
            Choice2.Visibility = Visibility.Hidden;
            Choice3.Visibility = Visibility.Hidden;
            Choice4.Visibility = Visibility.Hidden;

            try
            {
                if (_page.AreChoicesRandom && _page.Choices.Count > 0)
                {
                    Choice1.Content = _page.RandomChoiceContent; //global page random choice
                    Choice1.Visibility = Visibility.Visible;
                    return;
                }

                if (_page.Choices[0] != null)
                {
                    Choice1.Content = _page.Choices[0].Content;
                    Choice1.Visibility = Visibility.Visible;
                }
                else
                {
                    Choice1.Visibility = Visibility.Hidden;
                }
                if (_page.Choices[1] != null)
                {
                    Choice2.Content = _page.Choices[1].Content;
                    Choice2.Visibility = Visibility.Visible;
                }
                else
                {
                    Choice2.Visibility = Visibility.Hidden;
                }
                if (_page.Choices[2] != null)
                {
                    Choice3.Content = _page.Choices[2].Content;
                    Choice3.Visibility = Visibility.Visible;
                }
                else
                {
                    Choice3.Visibility = Visibility.Hidden;
                }
                if (_page.Choices[3] != null)
                {
                    Choice4.Content = _page.Choices[3].Content;
                    Choice4.Visibility = Visibility.Visible;
                }
                else
                {
                    Choice4.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception)
            {
                return;
            }
        }

        private async void browser_Initialized(object sender, EventArgs e)
        {
            await Task.Delay(1000);
            browser.NavigateToString(editSource.Text);
        }
    }
}
