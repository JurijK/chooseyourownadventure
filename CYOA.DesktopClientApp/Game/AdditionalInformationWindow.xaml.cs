﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CYOA.DesktopClientApp.Game
{
    /// <summary>
    /// Interaction logic for AdditionalInformationWindow.xaml
    /// </summary>
    public partial class AdditionalInformationWindow : Window
    {
        private readonly Library.Objects.Adventure _adventure;

        public AdditionalInformationWindow()
        {
            InitializeComponent();
        }

        public AdditionalInformationWindow(Library.Objects.Adventure adventure) : this()
        {
            _adventure = adventure;
        }

        private void ConfirmButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(_adventure.MCName) && string.IsNullOrWhiteSpace(Name.Text))
            {
                MessageBox.Show("You didn't specified your character name.\nTry one more time.");
                return;
            }

            _adventure.MCName = Name.Text;
            _adventure.IsMCFemale = Gender.IsChecked ?? false;

            GameWindow game = new GameWindow(_adventure)
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };

            try
            {
                game.Show();
            }
            catch (Exception ex)
            {
                Utils.HandleException(ex, this, false);
            }

            this.Close();
        }

        private void ReturnButton_Click(object sender, RoutedEventArgs e)
        {
            AdventureSelectWindow adventureSelection = new AdventureSelectWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            adventureSelection.Show();
            this.Close();
        }
    }
}
