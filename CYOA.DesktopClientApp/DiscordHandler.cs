﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordRPC;
using DiscordRPC.Logging;

namespace CYOA.DesktopClientApp
{
    public class DiscordHandler
    {
        public static readonly DiscordRpcClient client;
        public static RichPresence RichPresence { get; set; }

        static DiscordHandler()
        {
            client = new DiscordRpcClient(Utils.AppDiscordId)
            {
                Logger = new NullLogger()
            };

            RichPresence = new RichPresence()
            {
                Details = "Lorem Ipsum",
                State = "Test",
            };
        }

        public static void Initialize()
        {
            if (client.IsInitialized)
            {
                return;
            }

            client.Initialize();
        }

        public static void UpdateRichPresence(string details, string state)
        {
            RichPresence.Details = details;
            RichPresence.State = state;

            client.SetPresence(RichPresence);
        }

        public static void UpdateRichPresenceAssets(string bigImage, string bigImageText, string smallImage, string smallImageText)
        {
            RichPresence.Assets = new Assets()
            {
                LargeImageKey = bigImage,
                LargeImageText = bigImageText,
                SmallImageKey = smallImage,
                SmallImageText = smallImageText
            };

            client.SetPresence(RichPresence);
        }
    }
}
