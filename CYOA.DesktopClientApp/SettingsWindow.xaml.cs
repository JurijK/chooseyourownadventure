﻿using CYOA.Library.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CYOA.DesktopClientApp
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        public SettingsWindow()
        {
            InitializeComponent();

            FontColor.Text = Profiles.GetProfileFontColor();
            BackgroudColor.Text = Profiles.GetProfileBackgroundColor();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            Profiles.SetProfileFontColor(FontColor.Text.Trim().ToUpper());
            Profiles.SetProfileBackgroundColor(BackgroudColor.Text.Trim().ToUpper());

            BrushConverter converter = new BrushConverter();

            MainWindow main = new MainWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = converter.ConvertFromString(Profiles.GetProfileFontColor()) as SolidColorBrush,
                Background = converter.ConvertFromString(Profiles.GetProfileBackgroundColor()) as SolidColorBrush,
                WindowState = this.WindowState
            };
            main.Show();
            this.Close();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow()
            {
                Top = this.Top,
                Left = this.Left,
                Foreground = this.Foreground,
                Background = this.Background,
                WindowState = this.WindowState
            };
            main.Show();
            this.Close();
        }
    }
}
