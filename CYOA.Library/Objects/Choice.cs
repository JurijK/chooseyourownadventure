﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYOA.Library.Objects
{
    public class Choice
    {
        public int Id { init; get; }

        public string Content { get; set; }

        public int Weight { get; set; }

        public int PointsToPageWithId { get; set; }
    }
}
