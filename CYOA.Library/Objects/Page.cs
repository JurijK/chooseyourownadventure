﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYOA.Library.Objects
{
    public class Page
    {
        public int Id { init; get; }

        public string DevelopmentCode { get; set; } //Unique identifier containing previous choices

        public string ImageName { get; set; } = "";

        public string SoundName { get; set; } = "";

        public double SoundVolume { get; set; } = 0.5;

        public bool AreChoicesRandom { get; set; } = false;

        public string RandomChoiceContent { get; set; } = "";

        public string Content { get; set; }

        public IList<Choice> Choices { init; get; }
    }
}
