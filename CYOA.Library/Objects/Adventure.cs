﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYOA.Library.Objects
{
    public class Adventure
    {
        public int Id { init; get; }

        public string Author { init; get; } //Add as separate object

        public string Name { init; get; }

        public string Language { init; get; }

        public string Code
        {
            get
            {
                return $"{Id}_{Name}_{Author}";
            }
        }

        public string MCName { get; set; } = "";

        public bool? IsMCFemale { get; set; } = null;

        public string AmbientMusic { get; set; } = "";

        public double AmbientVolume { get; set; } = 0.5;

        public string ForAppVersion { get; set; } = "";

        public int MinimalAge { get; set; }

        public IList<Page> Pages { init; get; }
    }
}
