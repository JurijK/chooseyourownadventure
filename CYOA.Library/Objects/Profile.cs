﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYOA.Library.Objects
{
    public class Profile
    {
        public string FontColor { get; set; } = "#49A2BA";

        public string BackgroundColor { get; set; } = "#403F3F";
    }
}
