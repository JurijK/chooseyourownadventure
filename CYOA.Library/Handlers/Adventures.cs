﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CYOA.Library.Objects;
using CYOA.Library.Storage;

namespace CYOA.Library.Handlers
{
    public class Adventures
    {
        private static readonly IStorage _storage;
        private static readonly string _folderPath;
        private static readonly IList<Adventure> _adventures;

        static Adventures()
        {
            _storage = new JsonStorage();

            _folderPath = "./Resources/Stories";

            if (!Directory.Exists(_folderPath))
            {
                Directory.CreateDirectory(_folderPath);
            }

            _adventures = GetAdventures();
        }

        /// <summary>
        /// Gets all adventures from files in _filePath
        /// </summary>
        /// <returns>
        /// IList with all adventures
        /// </returns>
        public static IList<Adventure> GetAdventures()
        {
            IList<Adventure> adventureFiles = new List<Adventure>();

            foreach (string directory in Directory.GetDirectories(_folderPath))
            {
                foreach (string file in Directory.GetFiles(directory))
                {
                    if (!file.EndsWith(".json"))
                    {
                        continue;
                    }

                    adventureFiles.Add(_storage.RestoreObject<Adventure>(file));
                }
            }

            return adventureFiles;
        }

        public static void Save(Adventure adventure)
        {
            _storage.StoreObject(adventure, $"{_folderPath}/{adventure.Code}", $"{adventure.Code}.json");
        }

        /// <summary>
        /// Gets adventure on specified index
        /// </summary>
        /// <param name="index">
        /// Index of adventure
        /// </param>
        /// <returns>
        /// Adventure on specified index
        /// </returns>
        /// <remarks>
        /// 0-besed indexing!
        /// </remarks>
        public static Adventure GetAdventure(int index)
        {
            return _adventures.ElementAtOrDefault(index);
        }

        /// <summary>
        /// Gets adventure with it's code
        /// </summary>
        /// <param name="code">
        /// Code of adventure in format: it'sId_it'sName_it'sAuthor
        /// </param>
        /// <returns>
        /// Adventure with given code
        /// </returns>
        public static Adventure GetAdventure(string code)
        {
            return _adventures.SingleOrDefault(a => a.Code == code);
        }

        /// <summary>
        /// Creates new adventure
        /// </summary>
        /// <param name="author">
        /// Author of the adventure
        /// </param>
        /// <param name="name">
        /// Title of the story
        /// </param>
        /// <param name="language">
        /// Language ISO of adventure
        /// </param>
        /// <param name="minimalAge">
        /// Minimal recomended age
        /// </param>
        /// <returns>
        /// Newly created adventure
        /// </returns>
        public static Adventure CreateAdventure(string author, string name, string language, int minimalAge,
                                                string mcName = "", bool? isMCFemale = null, string music = "", double volume = 0.5,
                                                string appVersion = "")
        {
            if (_adventures.FirstOrDefault(a => a.Author == author && a.Name == name) != null)
            {
                throw new ArgumentException($"There already is adventure named: {name}");
            }

            Adventure adventure = new Adventure()
            {
                Id = _adventures.Count > 0 ? _adventures.Max(a => a.Id) + 1 : 1,
                Author = author,
                Name = name,
                Language = language,
                MCName = mcName,
                IsMCFemale = isMCFemale,
                MinimalAge = minimalAge,
                AmbientMusic = music,
                AmbientVolume = volume,
                ForAppVersion = appVersion,
                Pages = new List<Page>()
            };

            _adventures.Add(adventure);
            Save(adventure);

            return adventure;
        }

        public static Adventure EditAppVersion(Adventure adventure, string appVersion)
        {
            adventure.ForAppVersion = appVersion;
            Save(adventure);

            return adventure;
        }
    }
}
