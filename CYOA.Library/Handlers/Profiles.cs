﻿using CYOA.Library.Objects;
using CYOA.Library.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYOA.Library.Handlers
{
    public class Profiles
    {
        private static readonly string _filePath;
        private static readonly IStorage _storage;
        private static readonly Profile _profile;

        static Profiles()
        {
            _filePath = "./Profile.json";
            _storage = new JsonStorage();

            if (_storage.FileExist(_filePath))
            {
                _profile = _storage.RestoreObject<Profile>(_filePath);
            }
            else
            {
                _profile = new Profile();
                Save();
            }
        }

        private static void Save()
        {
            _storage.StoreObject(_profile, _filePath);
        }

        public static string GetProfileFontColor()
        {
            return _profile.FontColor;
        }

        public static void SetProfileFontColor(string color)
        {
            if (!color.StartsWith('#'))
            {
                color = "#" + color;
            }

            _profile.FontColor = color;

            Save();
        }

        public static string GetProfileBackgroundColor()
        {
            return _profile.BackgroundColor;
        }

        public static void SetProfileBackgroundColor(string color)
        {
            if (!color.StartsWith('#'))
            {
                color = "#" + color;
            }

            _profile.BackgroundColor = color;

            Save();
        }
    }
}
