﻿using System.IO;
using Newtonsoft.Json;

namespace CYOA.Library.Storage
{
    public class JsonStorage : IStorage
    {
        /// <inheritdoc/>
        public T RestoreObject<T>(string filePath)
        {
            string json = File.ReadAllText(filePath);
            return JsonConvert.DeserializeObject<T>(json);
        }

        /// <inheritdoc/>
        public void StoreObject(object obj, string filePath)
        {
            string json = JsonConvert.SerializeObject(obj, Formatting.Indented);

            File.WriteAllText(filePath, json);
        }

        /// <inheritdoc/>
        public void StoreObject(object obj, string folderPath, string filePath)
        {
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
                Directory.CreateDirectory($"{folderPath}/Images");
                Directory.CreateDirectory($"{folderPath}/Sounds");
            }

            string json = JsonConvert.SerializeObject(obj, Formatting.Indented);

            File.WriteAllText($"{folderPath}/{filePath}", json);
        }

        /// <inheritdoc/>
        public bool FileExist(string filePath)
        {
            return File.Exists(filePath);
        }
    }
}
