﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYOA.Library.Storage
{
    public interface IStorage
    {
        /// <summary>
        /// Get item of type T from file
        /// </summary>
        /// <typeparam name="T">
        /// Type of returning item
        /// </typeparam>
        /// <param name="filePath">
        /// Path to file
        /// </param>
        /// <returns>
        /// Object of type T
        /// </returns>
        public T RestoreObject<T>(string filePath);

        /// <summary>
        /// Store object inside a file
        /// </summary>
        /// <param name="obj">
        /// Object to store
        /// </param>
        /// <param name="filePath">
        /// Path to file
        /// </param>
        public void StoreObject(object obj, string filePath);

        /// <summary>
        /// Store object inside a file
        /// </summary>
        /// <param name="obj">
        /// Object to store
        /// </param>
        /// <param name="filePath">
        /// Path to file
        /// </param>
        public void StoreObject(object obj, string folderPath, string filePath);

        /// <summary>
        /// Check if file exist
        /// </summary>
        /// <param name="filePath">
        /// Path to file
        /// </param>
        /// <returns>
        /// true - if file already exist
        /// </returns>
        public bool FileExist(string filePath);
    }
}
