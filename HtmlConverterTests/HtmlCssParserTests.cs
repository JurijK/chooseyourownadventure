﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace HtmlConverter.Tests
{
    public class HtmlCssParserTests
    {
        [Test]
        public void ParseColor_WhenCalledWithRgbColor_ReturnHexColor()
        {
            int next = 0;
            Assert.AreEqual("#ff0000", HtmlConverter.HtmlCssParser.ParseColor("rgb(255, 0, 0)", ref next));
        }

        [Test]
        public void RgbToHex_WhenCalledWithRgbColor_ReturnHexColor()
        {
            Assert.AreEqual("#ff0000", HtmlConverter.HtmlCssParser.RgbToHex("255", "0", "0"));
        }
    }
}
